package geometry;
//Julian Mora 
public interface Shape {
	
	double getArea();
	
	double getPerimeter();
	
}

