package geometry;
//Julian Mora 
public class Circle implements Shape {

	private double radius;
	
	public Circle(double VRadius) {
		this.radius = VRadius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return Math.PI*Math.pow(this.radius,2);
	}
	
	public double getPerimeter() {
		return 2*Math.PI*this.radius;
	}
	
}
