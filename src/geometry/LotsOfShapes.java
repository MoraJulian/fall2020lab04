package geometry;
//Julian Mora
public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapeCollection = new Shape[5];
		shapeCollection[0] = new Rectangle(2.0,7.0);
		shapeCollection[1] = new Rectangle(4.5,9.3);
		shapeCollection[2] = new Circle(5.0);
		shapeCollection[3] = new Circle(9.3);
		shapeCollection[4] = new Square(4.1);
	
		for(Shape i: shapeCollection) {
			System.out.println("Shapes Area: " +i.getArea());
			System.out.println("Shapes Perimeter: " +i.getPerimeter());
			System.out.println();
		}
	}
}
