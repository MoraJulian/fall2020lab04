package geometry;
//Julian Mora
public class Rectangle implements Shape{
	
	private double length;
	private double width;
	
	public Rectangle(double VLength, double VWidth) {
		this.length = VLength;
		this.width = VWidth;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getPerimeter() {
		return this.length*2 + this.width*2;
	}
	
	public double getArea() {
		return this.length*this.width;
	}
	
	
}
