package inheritance;
//Julian Mora

public class ElectronicBook extends Book {
	private double numberBytes;
	
	public ElectronicBook(String VTitle, String VAuthor, double VNumberBytes) {
		super(VTitle,VAuthor);
		this.numberBytes = VNumberBytes;

	}

	public String toString() {
		return super.toString()+ "\nNumber of Bytes taken for storage: " +this.numberBytes;
	}
	

}


