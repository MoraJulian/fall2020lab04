package inheritance;
// Julian Mora 

public class BookStore {
	
	public static void main(String[] args) {
		
		Book[] bookCollection = new Book[5];
		bookCollection[0] = new Book("Dune","Frank Herbert");
		bookCollection[1] = new ElectronicBook("House of Leaves","Mark LongName",4.0);
		bookCollection[2] = new Book("Flowers For Algernon","Daniel Keyes");
		bookCollection[3] = new ElectronicBook("I Am Legend","Richard Matheson",3.0);
		bookCollection[4] = new ElectronicBook("The Road","Cormac Mcarthy",3.0);
		
		for(Book i: bookCollection) {
				System.out.println(i.toString());
				System.out.println();
		}
		
	}
	
}
