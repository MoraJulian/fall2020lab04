package inheritance;
//Julian Mora

public class Book {
	protected String title;
	private String author;
	
	public String getTitle() {
		return this.title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	
	public Book(String VTitle, String VAuthor) {
		this.title = VTitle;
		this.author = VAuthor;	
	}
	
	public String toString() {
		return "Book title: " +this.title+ "\nBook Author: " +this.author;
	}

	
	
}
